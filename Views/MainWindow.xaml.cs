﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using WeatherApp.ViewModels;
using WeatherApp.Views;

namespace WeatherApp
{

    public partial class MainWindow : Window
    {  
                public MainWindow()
        {            
            InitializeComponent();           
            MainFrame.Content = new MainPage();
            App.MainFrame = MainFrame;
        }

        private void MainFrame_Navigated(object sender, NavigationEventArgs e)
        {

        }
    }
}
